[![Godot Version](http://img.shields.io/badge/Godot-3.3.2-478cbf.svg?style=flat&colorA=8F8F8F)](https://downloads.tuxfamily.org/godotengine/3.3.2/)

Star-runner
===========

Installation
------------
This project use [git-lfs](https://git-lfs.github.com/). Make sure to initialize it.

Complete instruction in the [Wiki](https://gitlab.com/denisrob23/star-runner/-/wikis/installation)
