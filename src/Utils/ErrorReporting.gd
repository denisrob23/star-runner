class_name ErrorReporting
extends Object

enum ErrorType { EVENT_CONNECT, TREE_CHANGE_SCENE, TREE_RELOAD_SCENE }

# error type enum ERROR and type enum ERROR_TYPE
static func report_error(error: int, type: int, node: Node) -> void:
	if error == OK:
		return

	if [ErrorType.TREE_CHANGE_SCENE, ErrorType.TREE_RELOAD_SCENE].has(type):
		var dialog := ConfirmationDialog.new()
		dialog.dialog_text = TranslationServer.translate("Error code: {type}/{error}").format(
			{"type": str(type), "error": str(error)}
		)
		node.add_child(dialog)
		dialog.popup_centered()

	#TODO send it to a issue manager (sentry?)
	# can be used: get_stack()
