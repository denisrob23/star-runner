class_name Checkpoint
extends Spatial

onready var spawner := $Spawner
onready var area := $Area
onready var next_checkpoint: Checkpoint


func _ready() -> void:
	ErrorReporting.report_error(
		area.connect("body_entered", self, "on_body_entered"),
		ErrorReporting.ErrorType.EVENT_CONNECT,
		get_tree().root
	)


func respawn_ship(ship: AbstractShipController) -> void:
	ship.global_transform.origin = spawner.global_transform.origin
	ship.reset()


func on_body_entered(body: Node):
	if body is AbstractShipController and body.next_checkpoint == self:
		body.last_checkpoint = self
		body.next_checkpoint = next_checkpoint
