extends Control

signal restart
signal menu

const LAP_TEMPLATE := "{current_lap}/{lap_max}"

var leaderboard_template = preload("res://src/Levels/Gui/LeaderBoardShip.tscn")
var leaderboard_by_player = {}

onready var gui_health := $Health
onready var gui_shield := $Shield
onready var gui_boost := $Boost
onready var gui_lap := $PlayerLap/Lap
onready var gui_leaderboard := $LeaderBoard
onready var gui_winner := $EndResult/Winner
onready var gui_loser := $EndResult/Loser
onready var gui_buttons := $EndResult/Buttons
onready var gui_restart_button := $EndResult/Buttons/Restart
onready var gui_menu_button := $EndResult/Buttons/Menu
onready var gui_counter := $EndResult/Counter
onready var gui_second := $EndResult/Counter/Second
onready var gui_milli_second := $EndResult/Counter/Milli/Control/MilliSecond


func _ready() -> void:
	ErrorReporting.report_error(
		gui_restart_button.connect("pressed", self, "_on_restart_pressed"),
		ErrorReporting.ErrorType.EVENT_CONNECT,
		get_tree().root
	)
	ErrorReporting.report_error(
		gui_menu_button.connect("pressed", self, "_on_menu_pressed"),
		ErrorReporting.ErrorType.EVENT_CONNECT,
		get_tree().root
	)


func set_health(value: float) -> void:
	gui_health.value = value


func set_shield(value: float) -> void:
	gui_shield.value = value


func set_boost(value: float) -> void:
	gui_boost.value = value


func set_player_lap(current: int, lap_max: int) -> void:
	gui_lap.text = LAP_TEMPLATE.format(
		{"current_lap": str(current), "lap_max": str(lap_max)}
	)


func show_winner() -> void:
	gui_buttons.visible = true
	gui_winner.visible = true


func show_loser(ship_finished: int) -> void:
	gui_buttons.visible = true
	gui_loser.get_node("place").text = str(ship_finished)
	gui_loser.visible = true


func hide_counter() -> void:
	gui_counter.visible = false


func set_timing(second: int, milli_second: int) -> void:
	gui_second.text = str(second)
	gui_milli_second.text = str(milli_second)


func init_ship(ship: AbstractShipController, lap_max: int) -> void:
	var l = leaderboard_template.instance()
	l.get_node("Name").text = ship.ship_name
	l.get_node("Lap").text = LAP_TEMPLATE.format(
		{"current_lap": str(0), "lap_max": str(lap_max)}
	)
	gui_leaderboard.add_child(l)
	leaderboard_by_player[ship] = l


func lap_changed(
	ship: AbstractShipController, all_ship: Array, lap_current: int, lap_max: int
) -> void:
	var l = leaderboard_by_player[ship]
	l.get_node("Lap").text = LAP_TEMPLATE.format(
		{"current_lap": str(lap_current), "lap_max": str(lap_max)}
	)

	var place = 0
	for s in all_ship:
		if s.lap >= lap_current:
			place += 1
	gui_leaderboard.move_child(l, place)


func _on_restart_pressed() -> void:
	emit_signal("restart")


func _on_menu_pressed() -> void:
	emit_signal("menu")
