class_name Level
extends Spatial

export (Array, NodePath) var checkpoints_path
export (Array, NodePath) var ships_path
export var player_path: NodePath
export var camera_path: NodePath
export var path: NodePath
export var lap_max: int
export var waiting_second := 5
export var activate_zoom := 3

var ships = []
var start := OS.get_ticks_msec()
var started := false

onready var player := get_node(player_path)
onready var camera := get_node(camera_path)
onready var gui := $Gui


func _ready() -> void:
	start = OS.get_ticks_msec()
	ErrorReporting.report_error(
		player.connect("armor_changed", self, "_on_armor_changed"),
		ErrorReporting.ErrorType.EVENT_CONNECT,
		get_tree().root
	)
	ErrorReporting.report_error(
		player.connect("shield_changed", self, "_on_shield_changed"),
		ErrorReporting.ErrorType.EVENT_CONNECT,
		get_tree().root
	)
	ErrorReporting.report_error(
		player.connect("boost_changed", self, "_on_boost_changed"),
		ErrorReporting.ErrorType.EVENT_CONNECT,
		get_tree().root
	)
	ErrorReporting.report_error(
		gui.connect("restart", self, "_on_restart_pressed"),
		ErrorReporting.ErrorType.EVENT_CONNECT,
		get_tree().root
	)
	ErrorReporting.report_error(
		gui.connect("menu", self, "_on_menu_pressed"),
		ErrorReporting.ErrorType.EVENT_CONNECT,
		get_tree().root
	)
	gui.set_player_lap(0, lap_max)

	var checkpoints = []
	for checkpoint_path in checkpoints_path:
		checkpoints.append(get_node(checkpoint_path))

	for checkpoint_index in checkpoints.size():
		var next_index = checkpoint_index + 1
		if next_index >= checkpoints.size():
			next_index = 0
		checkpoints[checkpoint_index].next_checkpoint = checkpoints[next_index]

	var circuit = get_node(path)
	for ship_path in ships_path:
		var ship = get_node(ship_path)
		ships.append(ship)
		ship.next_checkpoint = checkpoints[0]
		ship.last_checkpoint = checkpoints[checkpoints.size() - 1]
		if ship is IAShipController:
			ship.circuit_path = circuit
		gui.init_ship(ship, lap_max)
		ErrorReporting.report_error(
			ship.connect("lap_changed", self, "_on_lap_changed"),
			ErrorReporting.ErrorType.EVENT_CONNECT,
			get_tree().root
		)


func _on_armor_changed(_value: float) -> void:
	gui.set_health((self.player.armor_pv / self.player.ship.armor_pv_max) * 100)


func _on_shield_changed(_value: float) -> void:
	gui.set_shield((self.player.shield_pv / self.player.ship.shield_pv_max) * 100)


func _on_boost_changed(_value: float) -> void:
	gui.set_boost((self.player.boost / self.player.ship.boost_max) * 100)


func _on_restart_pressed() -> void:
	ErrorReporting.report_error(
		get_tree().reload_current_scene(),
		ErrorReporting.ErrorType.TREE_RELOAD_SCENE,
		get_tree().root
	)


func _on_menu_pressed() -> void:
	ErrorReporting.report_error(
		get_tree().change_scene("res://src/Menu/MainMenu.tscn"),
		ErrorReporting.ErrorType.TREE_CHANGE_SCENE,
		get_tree().root
	)


func _on_lap_changed(sender: AbstractShipController, value: float) -> void:
	if sender == player:
		gui.set_player_lap(value, lap_max)
		if value == lap_max:
			var ship_finished = 0
			for ship in ships:
				if ship.lap >= lap_max:
					ship_finished += 1
			if ship_finished == 1:  # since 1 have already finished
				gui.show_winner()
			else:
				gui.show_loser(ship_finished)

	gui.lap_changed(sender, ships, value, lap_max)


func _process(_delta: float) -> void:
	if not started:
		var current := OS.get_ticks_msec()
		var second_passed = float(current - start) / 1000
		gui.set_timing(
			waiting_second - int(second_passed),
			99 - int((second_passed - int(second_passed)) * 100)
		)
		if current - start > 1000 * waiting_second:
			started = true
			gui.hide_counter()
			for ship in ships:
				ship.stop_trust_input = false
		if second_passed > activate_zoom:
			camera.active = true
