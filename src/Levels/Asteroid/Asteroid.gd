extends HitRigidBody

export var torque_random := 20.0
export var pv := 50.0 setget set_pv

export (Array, PackedScene) var parts

var active = false


func _ready() -> void:
	self.add_torque(
		Vector3(
			(randf() - 0.5) * torque_random,
			(randf() - 0.5) * torque_random,
			(randf() - 0.5) * torque_random
		)
	)
	if has_node("SpawTimer"):
		ErrorReporting.report_error(
			get_node("SpawTimer").connect("timeout", self, "_on_spawn_timer"),
			ErrorReporting.ErrorType.EVENT_CONNECT,
			get_tree().root
		)
	else:
		active = true


func register_hit(damage: float) -> void:
	self.pv -= damage


func set_pv(value: float) -> void:
	if active and pv > 0 and value <= 0:
		if parts:
			for part in parts:
				var instance_part = part.instance()
				instance_part.global_transform = self.global_transform
				get_parent().add_child(instance_part)
		self.queue_free()
	pv = value


func _on_spawn_timer() -> void:
	active = true
