class_name GraphicalShip
extends Spatial

export var acceleration_forward := 10.0
export var shield_pv_max := 50.0
export var shield_reload_rate := 10.0
export var armor_pv_max := 50.0
export var boost_max := 100.0
export var boost_multiplier := 4.0
export var shield_reload_timer := 5.0
export var respawn_timer := 3.0

# only for the menu, will be overwrite by the controler
export var shield_pv: float = shield_pv_max setget set_shield_pv
export var armor_pv: float = armor_pv_max setget set_armor_pv
export var boost: float = boost_max setget set_boost
export var truster: float = 0.0 setget set_truster

var collision_shield := []
var collision_ship := []

onready var animations := $AnimationTree
onready var parent_collision_ship := $ShipCollision
onready var parent_collision_shield := $ShieldCollision


func _ready() -> void:
	collision_ship = parent_collision_ship.get_children()
	collision_shield = parent_collision_shield.get_children()


func retreive_collision() -> void:
	for n in collision_ship:
		n.get_parent().remove_child(n)
		self.parent_collision_ship.add_child(n)
	for n in collision_shield:
		n.get_parent().remove_child(n)
		self.parent_collision_shield.add_child(n)


func set_shield_pv(value: float) -> void:
	shield_pv = value
	self.animate("parameters/Shield/blend_position", self.shield_pv / self.shield_pv_max)


func set_armor_pv(value: float) -> void:
	armor_pv = value
	self.animate("parameters/Hull/blend_position", self.armor_pv / self.armor_pv_max)


func set_boost(value: float) -> void:
	boost = value


func set_truster(value: float) -> void:
	truster = value
	self.animate("parameters/Truster/blend_position", self.truster)


func death() -> void:
	self.armor_pv = self.armor_pv_max
	self.truster = 0.0
	self.animate("parameters/Destruction/active", true)


func reset() -> void:
	self.animate("parameters/Spawn/active", true)


func animate(param: String, value) -> void:
	self.animations.set(param, value)
