class_name AbstractShipController
extends HitRigidBody

signal shield_changed(value)
signal armor_changed(value)
signal boost_changed(value)
signal lap_changed(sender, value)

export var ship_name := ""
export var ship_package: PackedScene

var stop_trust_input := true
var lap := 0 setget set_lap
var is_dead := false
var shield_pv := 0.0 setget set_shield_pv
var armor_pv := 0.0 setget set_armor_pv
var boost := 0.0 setget set_boost
var shield_reloading := false
var collision_shield := []
var collision_ship := []

onready var last_checkpoint: Node
onready var next_checkpoint: Node setget set_next_checkpoint
onready var ship: GraphicalShip
onready var shield_timer := $ShieldTimer
onready var respawn_timer := $RespawnTimer


func _ready() -> void:
	ErrorReporting.report_error(
		self.shield_timer.connect("timeout", self, "on_shield_ready"),
		ErrorReporting.ErrorType.EVENT_CONNECT,
		get_tree().root
	)
	ErrorReporting.report_error(
		self.respawn_timer.connect("timeout", self, "respawn"),
		ErrorReporting.ErrorType.EVENT_CONNECT,
		get_tree().root
	)
	self.get_node("ShipPlaceholder").free()
	self.load_ship(ship_package.instance())


func get_target() -> Vector3:
	return Vector3.ZERO


func get_truster_strength() -> float:
	return 0.0


func get_boost() -> bool:
	return false


func set_next_checkpoint(value) -> void:
	next_checkpoint = value


func set_lap(value: int) -> void:
	lap = value
	emit_signal("lap_changed", self, lap)


func set_shield_pv(value: float) -> void:
	if shield_pv > value:
		shield_reloading = false
		self.shield_timer.stop()
	shield_pv = min(max(value, 0), self.ship.shield_pv_max)
	if shield_pv == 0.0:
		for n in self.collision_shield:
			n.disabled = true
	else:
		for n in self.collision_shield:
			n.disabled = false
	if shield_pv == self.ship.shield_pv_max:
		shield_reloading = false
	elif (
		shield_pv < self.ship.shield_pv_max
		and self.shield_timer.is_stopped()
		and not shield_reloading
	):
		self.shield_timer.start()
	self.ship.shield_pv = shield_pv
	emit_signal("shield_changed", shield_pv)


func set_armor_pv(value: float) -> void:
	armor_pv = value
	self.ship.armor_pv = armor_pv
	if armor_pv <= 0:
		death()
	emit_signal("armor_changed", armor_pv)


func set_boost(value: float) -> void:
	boost = value
	if boost < 0:
		boost = 0
	self.ship.boost = boost
	emit_signal("boost_changed", boost)


func _process(delta: float) -> void:
	if self.is_dead:
		return
	if self.shield_reloading:
		self.shield_pv += self.ship.shield_reload_rate * delta
	if not get_boost() and self.boost < self.ship.boost_max:
		self.boost += delta * 10


func _physics_process(delta: float) -> void:
	if is_dead:
		return
	var move_force = Vector3.ZERO
	var truster_strength = get_truster_strength()
	if truster_strength and not stop_trust_input:
		move_force = (
			Vector3.FORWARD.rotated(Vector3.UP, self.rotation.y)
			* truster_strength
			* self.ship.acceleration_forward
		)
		#var boosted = false TODO
		if get_boost() and self.boost > 0:
			move_force *= ship.boost_multiplier
			self.boost -= delta * 100
			#boosted = true
		self.add_central_force(move_force)
		self.ship.truster = truster_strength
	else:
		self.ship.truster = 0.0


func look_follow(
	state: PhysicsDirectBodyState, current_transform: Transform, target_position: Vector3
):
	var up_dir := Vector3(0, 1, 0)
	var cur_dir := current_transform.basis.xform(Vector3(0, 0, 1))
	var target_dir := (target_position - current_transform.origin).normalized()
	var rotation_angle = -(Vector2(cur_dir.x, cur_dir.z).angle_to(
		-Vector2(target_dir.x, target_dir.z)
	))
	state.angular_velocity.y = (up_dir * rotation_angle / state.get_step()).y


func _integrate_forces(state: PhysicsDirectBodyState):
	if is_dead:
		return
	var target := get_target()
	look_follow(state, get_global_transform(), target)
	._integrate_forces(state)


func register_hit(damage: float) -> void:
	if self.shield_pv > 0:
		self.shield_pv = self.shield_pv - damage
		if self.shield_pv < 0:
			self.shield_pv = 0
	else:
		self.armor_pv = self.armor_pv - damage


func load_ship(ship_to_load: GraphicalShip) -> void:
	if self.ship:
		self.ship.retreive_collision()
		self.remove_child(self.ship)
		self.ship.free()
	self.add_child(ship_to_load)
	self.ship = ship_to_load
	self.respawn_timer.wait_time = self.ship.shield_reload_timer
	self.shield_timer.wait_time = self.ship.respawn_timer
	self.collision_shield = ship_to_load.collision_shield
	self.collision_ship = ship_to_load.collision_ship
	for n in self.collision_shield:
		n.get_parent().remove_child(n)
		self.add_child(n)
	for n in self.collision_ship:
		n.get_parent().remove_child(n)
		self.add_child(n)
	self.reset_values()


func on_shield_ready() -> void:
	shield_reloading = true


func respawn() -> void:
	last_checkpoint.respawn_ship(self)


func death() -> void:
	self.ship.death()
	is_dead = true
	respawn_timer.start()
	for n in self.collision_shield:
		n.disabled = true
	for n in self.collision_ship:
		n.disabled = true


func reset() -> void:
	is_dead = false
	for n in self.collision_shield:
		n.disabled = false
	for n in self.collision_ship:
		n.disabled = false
	self.linear_velocity = Vector3.ZERO
	self.ship.reset()
	self.reset_values()


func reset_values() -> void:
	self.armor_pv = self.ship.armor_pv_max
	self.shield_pv = self.ship.shield_pv_max
	self.boost = self.ship.boost_max
