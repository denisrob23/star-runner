extends Spatial

onready var play_button = $MainMenu/VBoxContainer/Button


func _ready() -> void:
	ErrorReporting.report_error(
		play_button.connect("pressed", self, "play_pressed"),
		ErrorReporting.ErrorType.EVENT_CONNECT,
		get_tree().root
	)


func play_pressed() -> void:
	ErrorReporting.report_error(
		get_tree().change_scene("res://src/Levels/Level1.tscn"),
		ErrorReporting.ErrorType.TREE_CHANGE_SCENE,
		get_tree().root
	)
