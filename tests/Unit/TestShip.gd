extends "res://addons/gut/test.gd"

var controller := preload("res://tests/Ressources/TestShipController.tscn")
var graphical_ship := preload("res://tests/Ressources/TestGraphicalShip.tscn")
var asteroid := preload("res://src/Levels/Asteroid/Asteroid.tscn")
var checkpoint := preload("res://src/Levels/Checkpoint/Checkpoint.tscn")


func test_load_ship() -> void:
	var c := controller.instance()
	var s1 := graphical_ship.instance()

	c.ship_package = graphical_ship
	c = add_child_autofree(c)

	assert_eq(c.shield_pv, 50.0)
	assert_eq(c.armor_pv, 50.0)
	assert_eq(c.boost, 100.0)

	s1.shield_pv_max = 100
	s1.armor_pv_max = 120
	s1.boost_max = 150

	c.load_ship(s1)

	assert_eq(c.shield_pv, 100.0)
	assert_eq(c.armor_pv, 120.0)
	assert_eq(c.boost, 150.0)

	assert_no_new_orphans()


func test_go_to_target() -> void:
	var c := controller.instance()

	c.ship_package = graphical_ship
	c = add_child_autofree(c)
	c.target = Vector3(10, 0, 5)
	c.stop_trust_input = false
	# must yield since it use the physics engine
	yield(yield_for(0.5), YIELD)
	# no truster yet
	assert_eq(c.transform.origin, Vector3.ZERO, "The ship shoudn't move without truster")

	c.truster_strength = 1.0
	c.stop_trust_input = true
	# must yield since it use the physics engine
	yield(yield_for(0.5), YIELD)
	# no truster yet
	assert_eq(c.transform.origin, Vector3.ZERO, "The ship shoud move with stop_trust_input eq true")

	c.stop_trust_input = false
	# must yield since it use the physics engine
	yield(yield_for(0.5), YIELD)
	# no truster yet
	assert_ne(c.transform.origin, Vector3.ZERO, "The ship shoud move with truster and target")

	assert_no_new_orphans()


func test_collision_and_respawn() -> void:
	var c := controller.instance()

	c.ship_package = graphical_ship
	c = add_child_autofree(c)
	c.respawn_timer.wait_time = 0.01
	c.shield_timer.wait_time = 0.1
	c.ship.acceleration_forward = 100
	c.ship.shield_reload_rate = 1000000000
	var shield_base = c.shield_pv
	var armor_base = c.armor_pv

	var a = asteroid.instance()
	a.transform.origin = Vector3(4.5, 0, 3.5)
	a.pv = 1
	a.parts = []
	a = add_child_autofree(a)

	var cp = checkpoint.instance()
	cp.transform.origin = Vector3(10, 0, 10)
	cp = add_child_autofree(cp)
	c.last_checkpoint = cp

	c.target = a.transform.origin
	c.truster_strength = 1.0
	c.stop_trust_input = false
	# must yield since it use the physics engine
	yield(yield_to(c, "shield_changed", 10), YIELD)
	assert_freed(a, "Asteriod")
	assert_ne(c.shield_pv, shield_base, "The ship sould have some damage")

	yield(yield_to(c.shield_timer, "timeout", 1), YIELD)
	simulate(c, 1, 1.0)
	assert_eq(c.shield_pv, shield_base, "The shield sould have reloaded")

	c.register_hit(shield_base + 100)
	assert_eq(c.armor_pv, armor_base, "The ship armor only have damage after the shield is off")

	c.register_hit(armor_base - 1)
	assert_eq(c.armor_pv, 1.0, "The ship armor didn't have any damage")

	c.register_hit(1)
	assert_eq(c.is_dead, true, "The ship should have died")

	# waiting for respawn
	yield(yield_to(c.respawn_timer, "timeout", 1), YIELD)
	assert_eq(c.is_dead, false, "The ship should have respawn")
	assert_eq(c.shield_pv, shield_base, "The ship armor should have reset")
	assert_eq(c.armor_pv, armor_base, "The ship armor should have reset")
	assert_no_new_orphans()
