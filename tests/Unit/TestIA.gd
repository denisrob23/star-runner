extends "res://addons/gut/test.gd"

var scene := preload("res://tests/Ressources/TestLevel.tscn")


func test_ia() -> void:
	var s = scene.instance()
	s.get_node("Gui").free()
	var gui = double("res://src/Levels/Gui/GameGui.tscn").instance()
	s.add_child(gui)
	gui.name = "Gui"
	add_child_autofree(s)
	gui.visible = false

	# must yield since it use the physics engine
	yield(yield_to(s.player, "lap_changed", 10), YIELD)
	assert_call_count(s.gui, 'lap_changed', 1)
	assert_call_count(s.gui, 'show_winner', 0)

	yield(yield_to(s.player, "lap_changed", 10), YIELD)
	assert_call_count(s.gui, 'lap_changed', 2)
	assert_call_count(s.gui, 'show_winner', 1)
	print_stray_nodes()

	assert_no_new_orphans()
