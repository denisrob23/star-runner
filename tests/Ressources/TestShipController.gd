extends AbstractShipController

var target := Vector3.ZERO
var truster_strength := 0.0
var boosted := false


func get_target() -> Vector3:
	return target


func get_truster_strength() -> float:
	return truster_strength


func get_boost() -> bool:
	return boosted
